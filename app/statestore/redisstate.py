from datetime import datetime
from json import dumps, loads
import redis


class RedisState(object):

    STATE_PREFIX = 'podstate'

    def __init__(self, **kw):
        hostname = kw.get('hostname', 'localhost')
        port = kw.get('port', 6379)
        db = kw.get('db', 0)
        self.node = kw.get('node')
        self.key = '{prefix}:{nodename}'.format(
            nodename=self.node,
            prefix=self.STATE_PREFIX
        )

        self.r = redis.Redis(host=hostname, port=port, db=db)


    def init_state(self):
        self.set_state({
            'nodename': self.node,
            'started': datetime.now().isoformat(),
            'page_loads': 0,
            'status': 'started'
        })

    def stop_state(self):
        state = self.get_state()
        state['status'] = 'stopped'
        state['last_updated'] = datetime.now().isoformat()
        self.set_state(state)


    def set_state(self, state):
        self.r.set(self.key, dumps(state))

    def get_state(self):
        return loads(self.r.get(self.key))

    def delete_state_by_key(self, key):
        self.r.delete(key)

    def get_state_by_key(self, key):
        return loads(self.r.get(key))

    def list_state(self):
        """ Iterates over keys, not state objects """
        return self.r.scan_iter('{prefix}:*'.format(
            prefix=self.STATE_PREFIX
        ))

