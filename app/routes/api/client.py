import os
import json
from platform import node
from flask import request, current_app, Blueprint, jsonify
from flask_restful import Resource, Api
from app.statestore import StateStore

client_ip_bp = Blueprint('client_ip', __name__)
client_api = Api(client_ip_bp, prefix='/api/v1/client')


class ClientIP(Resource):

    def get(self):
        """
        Show client IP info
        """

        client_data = {
            'ip': request.remote_addr,
            'nodename': str(node())
        }

        if current_app.config['STORE_STATE'] is True:
            ss = StateStore(
                hostname=os.getenv('REDIS_HOST'),
                node=node()
            )
            state = ss.get_state()
            state['page_loads'] = state['page_loads']+1
            ss.set_state(state)

            client_data['states'] = []
            for key in ss.list_state():
                client_data['states'].append(ss.get_state_by_key(key))

        return jsonify(client_data)


class PurgeOldState(Resource):

    def post(self):
        if current_app.config['STORE_STATE'] is False:
            return jsonify({
                'status': 'State storing is not enabled'
            }, 500)

        ss = StateStore(
            hostname=os.getenv('REDIS_HOST'),
            node=node()
        )
        deleted_states = 0
        for key in ss.list_state():
            state = ss.get_state_by_key(key)
            if state['status'] == 'stopped':
                ss.delete_state_by_key(key)
                deleted_states = deleted_states+1

        return jsonify({
            'status': 'Deleted {states} states'.format(
                states=deleted_states
            )
        })


client_api.add_resource(ClientIP, '/ip')
client_api.add_resource(PurgeOldState, '/purge')
